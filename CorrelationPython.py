import math
import matplotlib.pyplot as plt

# maliste = [24,24,25,26,28,55,56,57]
maliste = [12,524,67,823,5,76,12,344]
# malisteDeux = [24,24,25,26,28,30,35,38]
malisteDeux = [26,40,66,78,18,30,4,38]
def findLongeurListe (liste):
    res = 0
    for i in liste:
        res += 1
    return res

def trie_swap(liste):
    for i in range(findLongeurListe(liste)):
        for j in range(1, findLongeurListe(liste)):
            x = liste[j-1]
            y = liste[j]
            if x > y:
                liste[j-1], liste[j] = y, x

### Médiane valeur discrette

def findMediane(liste):
    n = findLongeurListe(liste)
    trie_swap(liste)
    index_mediane = (n + 1) / 2
    if (n+1) % 2 != 0:
        x = int(index_mediane - 0.5) - 1
        y = int(index_mediane + 0.5) - 1
        mediane = (liste[x] + liste[y]) / 2
    else:
        mediane = liste[int(index_mediane) - 1]
    print("La mediane de la liste est de : " + str(mediane))

findMediane(maliste)

### Esperance
def esperance(liste):
    n=0
    somme=0.0
    for i in liste:
        somme += i
        n+=1
    return somme / n

print("l'esperance de la liste est de : " + str(esperance(maliste)))

### Variance
def findVariance (liste):
    buffer = 0
    n = findLongeurListe(liste)
    for number in liste:
        buffer += number
    x_barre = buffer/n
    variancebuffer = 0
    for number in liste:
        variancebuffer += (number - x_barre) * (number - x_barre)
    variance = variancebuffer * (1/(n -1))
    return variance

print("la variance de la liste est de : ", findVariance(maliste))

### Ecart-Type

def ecart_type(liste):
    return math.sqrt(findVariance(liste))

print("l' ecart-type de la liste est de : ", ecart_type(maliste))


def findCovariance(listeUn, listeDeux):
    xBar = esperance(listeUn)
    yBar = esperance(listeUn)
    n1 = findLongeurListe(listeUn)
    n2 = findLongeurListe(listeDeux)
    if n1 != n2:
        return print("Liste de taille diffrente")
    covariance = 0
    for x,y in zip(listeUn,listeDeux):
        covariance += (x - xBar) * (y - yBar) * (1/(n1 - 1))
    return covariance


def findCorelation(listeUn, listeDeux):
    n1 = findLongeurListe(listeUn)
    n2 = findLongeurListe(listeDeux)
    if n1 != n2:
        return print("Liste de taille diffrente")
    covariance = findCovariance(listeUn, listeDeux)
    sigmaX = ecart_type(listeUn)
    sigmaY = ecart_type(listeDeux)
    corelation = covariance / (sigmaX * sigmaY)
    return corelation

print("l' Covariance de la liste est de : " +str(findCovariance(maliste,malisteDeux)))
#
print("le facteur de correlation de la liste est de : " +str(findCorelation(maliste,malisteDeux)))




# plt.scatter(maliste,malisteDeux,color="red")
# plt.show()